> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development 

## Adam Michia

### Assignment 5 Requirements:

*Four Parts:*

1. Create ConnectionPool.java, CustomerDB.java, and DBUtil.java.
2. Compile all servlets. 
3. Provide screenshots of custoemrform.jsp, thanks.jsp, and database entry.
4. Provide skillsets 13-15.

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Associated Database Entry
* Screenshots of skillsets 13-15

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

| *Valid User Form Entry (customerform.jsp)*:  | *Passed Validation (thanks.jsp)*: |
|---|---|
| ![Valid Entry](img/a.png)  | ![Passed Validation](img/b.png)  | 


*Screenshot of Associated Database Entry*:

![DB Entry](img/db.png)

#### Skillset Screenshots:
| *Screenshot of SS13*:  | *Screenshot of SS14*: |*Screenshot of SS15*: |
|---|---|---|
| ![SS13 Screenshot](img/ss13.png)  | ![SS14 Screenshot](img/ss14.png)  |  ![SS15 Screenshot](img/ss15.png) |
  
