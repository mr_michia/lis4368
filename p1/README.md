> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Adam Michia

### Project #1 Requirements:

*Sub-Heading:*

    1.  Review cloned lis4368_student_files Bitbucket repo subdirectories and files: 
        a. Review subdirectories and files, especially META-INF and WEB-INF. 
        b. Each assignment *must* have its own global subdirectory.
    2. Open index.jsp and review code:
        a. Suitably modify meta tags
        b. Change title, navigation links, and header tags appropriately
        c. Add form controls to match attributes of customer entity
        d. Add the following jQuery validation and regular expressions-- as per the entity attribute requirements (and screenshots below):
            i. *All* input fields, except Notes are required
            ii. Use min/max jQuery validation
            iii. Use regexp to only allow appropriate characters for each control:
                cus_fname, cus_lname, cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email, cus_balance, cus_total_sales, cus_notes
                fname, lname: provided
                street, city:
                no more than 30 characters
                Street: must only contain letters, numbers, commas, hyphens, or periods
                City: can only contain letters, numbers, hyphens, and space character (29 Palms) state:
                must be 2 characters
                must only contain letters
                zip:
                must be between 5 and 9 characters, inclusive
                must only contain numbers
                phone:
                must be 10 characters, including area code
                must only contain numbers
                email: provided (also, see below: Lesson 7)
                balance, total_sales:
                no more than 6 digits, including decimal point
                can only contain numbers, and decimal point (if used)
            
        iv. *After* testing jQuery validation, use HTML5 property to limit the number of characters for each control
        v. Research what the following validation code does:
        valid: 'fa fa-check',
        invalid: 'fa fa-times',
        validating: 'fa fa-refresh'
        Use git to push *all* p1 files and changes to your remote course Bitbucket repo 

        e. Add the following jQuery validation and regular expressions-- as per the entity attribute requirements (and screenshots below):


    3. Use the following instructions:
        a. Form ValidationPlugin: http://formvalidation.io/getting-started/
        b. Lesson 6 - Remote Repos: Initial push(w/oclone) http://www.qcitr.com/usefullinks.htm#lesson6
        c. Lesson 7 - Regular Expressions http://www.qcitr.com/usefullinks.htm#lesson7


#### README.md file should include the following items:

1. Screenshots of
    - LIS4368 Portal (Main/Splash Page)
    - Failed Validation
    - Passed Validation

#### Assignment Screenshots:

*Screenshot of LIS4368 Portal (Main/Splash Page)*:

![LIS4368 Portal (Main/Splash Page) Screenshot](img/home.png)



*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failed.png)


*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)


#### Skillset Screenshots:

*Screenshot of pset7.png running:*

![pset7.png Screenshot](img/pset7.png)

*Screenshot of pset8.png running:* 

![pset8-1.png Screenshot](img/pset8-1.png)
![pset8-2.png Screenshot](img/pset8-2.png)
![pset8-3.png Screenshot](img/pset8-3.png)


*Screenshot of pset9.png running:* 

![pset9.png Screenshot](img/pset9.png)