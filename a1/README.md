> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Adam Michia

### Assignment #1 Requirements:

*Sub-Heading:*

1. Screenshot of running java Hello (#1 above); 
2. Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial); 
3. Screenshot of a1/index.jsp 
4. git commands w/short descriptions; 
5. Bitbucket repo links: 
a. This assignment, and  
b. The completed tutorial repo above (bitbucketstationlocations).  
(See link in screenshot below.)
#### README.md file should include the following items:


1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/tomcat.png)



*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mr_michia/lis4368/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mr_michia/lis4368/src/master/README.md/ "My Team Quotes Tutorial")