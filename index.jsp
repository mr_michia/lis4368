<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Adam Michia">
	<link rel="icon" href="favicon.ico">


	  <!-- =====BOX ICONS===== -->
	  <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
	  <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
	  <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>

	<title>Adam Michia's Website</title>

	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>
	
</head>
<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="3000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

				 <div class="active item" style="background: url(img/blk.jpeg) no-repeat left center; background-size: cover;">
					<h2 style="color: rgb(154, 42, 42)">Stay Connected Below</h2> 
					<div class="container">
						
						 <div class="carousel-caption">
							 <p class="lead">
							 <span class="iconify" data-icon="akar-icons:arrow-down-thick"></span>
							 <span class="iconify" data-icon="akar-icons:arrow-down-thick"></span>
							 <span class="iconify" data-icon="akar-icons:arrow-down-thick"></span>
							</p>
							 <div class="lead">
								<a href="https://www.linkedin.com/in/adam-michia/" target="_blank"  ><i class='bx bxl-linkedin'></i></a>
								<a href="https://github.com/MrMichia" target="_blank"  ><i class='bx bxl-github' ></i></a>
								<a href="mailto:agm18r@my.fsu.edu" target="_blank"  ><i class='bx bx-envelope' ></i></a>
							</div>
							 <a class="btn btn-large btn-primary" href="https://mrmichia.github.io" target="_blank">Personal Website<span class="iconify" data-icon="akar-icons:arrow-forward-thick"></span></a>
						 </div>
					 </div>
				 </div>					

         <div class="item" style="background: url(img/microsoft.jpeg) no-repeat left center; background-size: cover;">
                <h2>Incoming Support Engineer</h2>
                <div class="carousel-caption">
                  <h4>Following my graduation from Florida State University, I will be working as a full-time Microsoft Support Engineer in Charlotte, NC.</h4>
						 <!--  <img src="img/slide2.png" alt="Slide 2">									 -->						
                </div>
            </div>

         <div class="item" style="background: url(img/fsu.jpeg) no-repeat left center; background-size: cover; position: center; margin-left: auto; margin-right: auto;">
                <div class="carousel-caption" style="color: black;">
                  <h3>Expected Graduation: Spring 22'<h3>
                  <p>I.T. Major GPA: 4.0</p>
						<!--  <img src="img/slide3.png" class="img-responsive" alt="Slide 3">							 -->								
                </div>
            </div>

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>
