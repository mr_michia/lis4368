import java.util.Scanner;
import java.io.File;

public class methods
{
    public static void getRequirements()
    {
    System.out.println("Developer: Adam Michia");
    System.out.println("Program lists files and subdirectories of user-specified directory");
    System.out.println(); // print blank line
    }

    public static void directoryInfo()
    {
        String userDir = "";
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter directory path: ");
        userDir = sc.nextLine();

        File directoryPath = new File(userDir);
        //Version 1: no exception-handling (not good!)
        //list files and subdirectories of specified directory

        File filesList[] = directoryPath.listFiles();
        System.out.println("List files and directories in specified directory:");
        for(File file : filesList)
            {
            System.out.println("Name: " + file.getName());
            System.out.println("Path: " + file.getAbsolutePath());
            System.out.println("Size (Bytes): " + file.length());
            System.out.println("Size (KB):" + file.length()/(1024));
            System.out.println("Size (MB): " + file.length()/ (1024*1024));
            System.out.println();
            }

            sc.close(); //close scanner
            
    }
}

