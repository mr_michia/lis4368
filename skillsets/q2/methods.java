import java.util.Scanner;

class methods 
{
    public static void getRequirements()
    {
    System.out.println("Author: Adam Michia");
    System.out.println("Program Requirements:");
    System.out.println("1. Program swaps two user-entered floating-point values.") ;
    System.out.println("2. Create at least two user-defined methods: getRequirements() and numberSwap().");
    System.out.println("3. Must perform data validation: numbers must be floats.");
    System.out.println("4. Print numbers before/after swapping.");
    }
    
    public static void arrayLoops() 
    {
        float myFloats[] = {1.0f, 2.1f, 3.2f, 4.3f, 5.4f};
        System.out.println("for loop:");
        for(int i = 0; i < myFloats.length; i++)
        {
        System.out.println(myFloats[i]);
        }

        //Note: enhanced for loop does not require an Iterator (e.g., × or i or j).
        //Loop iterates through each element of array/collection.
        System.out.println("\nEnhanced for loop:");
            for (float test : myFloats)
            {
            System.out.println(test);
            }
        
        System.out.println("\nwhile loop:");
        int i=0;

            while (i < myFloats.length)
            {
            System.out.println(myFloats[i]);
            i++;
            }

        i=0; //reassign 0 to test variable

        System.out.println("\ndo…..while loop:");
            do
            {
            System.out.println(myFloats[i]);
            i++;
            }
            while (i < myFloats.length);
    }
}
  