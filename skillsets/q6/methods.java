import java.util.Scanner;

public class Methods
{
    //create method without returning any value (without object)
    public static void getRequirements()
{
    //Display operational messages
    System.out.println("Developer: Adam Michia");
    System.out.println("Program determines whether user-entered value is vowel, consonant, special character, or integer.");
    System.out.println("Program displays character\'s ASCII value.");

    System.out.println("\nReferences:\n"
            + "ASCII Background: https://urldefense.com/v3/__https://en.wikipedia.org/wiki/ASCII*5Cn__;JQ!!Epnw_ITfSMW4!7z1WWCtCSbKtRA6SJGXbEZSUFt09r9IZeQ_DRAh6TmQjzf0x_3771Y5H2ioMBzO5LA$ "
            + "ASCII Character Table: https://urldefense.com/v3/__https://ascii-code.com/*5Cn__;JQ!!Epnw_ITfSMW4!7z1WWCtCSbKtRA6SJGXbEZSUFt09r9IZeQ_DRAh6TmQjzf0x_3771Y5H2ipxLqfV6g$ "
            + "Lookup Tables: https://urldefense.com/v3/__https://www.lookuptables.com/__;!!Epnw_ITfSMW4!7z1WWCtCSbKtRA6SJGXbEZSUFt09r9IZeQ_DRAh6TmQjzf0x_3771Y5H2irvqDANJQ$ ");
    System.out.println(); //print blank line
}

public static void determineChar()
{
    //initialize variables
    char ch = ' '; //unlike empty string *must* include character -- here. space character!
    char chTest = ' '; //ch cast to lower-case
    Scanner sc = new Scanner(System.in);

    System.out.print("Please enter character: ");
    //next() function returns next token
    //Token: smallest element of a program meaningful to compiler/interpreter
    //Generally, identifiers keywords, literals, operators, and punctuations
    //Note: white space and comments not tokens - thought, separate tokens
    //Example: "I like this" ("I" is 1st token, "like" is second token, and "this" is third token)
    ch = sc.next().charAt(0); //capture first character from first token
    chTest = Character.toLowerCase(ch);

    //Note: Can be resolved in different ways.
    //Here: Works with legacy Java, no need for special functions...
    //test for alpha characters
    if((chTest == 'a' || chTest == 'e' || chTest == 'i' || chTest == 'o' || chTest == 'u' || chTest == 'y'))

    {
        System.out.println(ch + " is a vowel. ASCII value: " + (int)ch); //cast char to int
    }
    //test for numeric characters
    else if (ch >= '0' && ch <= '9')
    {
        System.out.println(ch + " is integer. ASCII value: " + (int)ch);
    }
    //test for consonats (note: vowels captured above)
    else if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
    {
        System.out.println(ch + " is a consonant. ASCII value: " + (int)ch);
    }
    //otherwise must be a special character
    else 
    {
        System.out.println(ch + " is a special character. ASCII value: " + (int)ch);
        System.out.println();

    }
    sc.close(); //close scanner
    }
}