import java.util.Scanner;

public class methods
{
    public static void getRequirements()
    {
    System.out.println("Developer: Adam Michia");
    System.out.println("1. Counts number and types of characters from user-entered string.");
    System.out.println("2. Count: total, letters (upper-/lower-case), numbers, spaces, and other characters.");
    System.out.println(); // print blank line

    }

    public static void getAscii() 
    {
    //intialize variables
        int num = 0;
        boolean IsValidNum = false;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Printing characters A-Z as ASCII values:");
        for(char character = 'A'; character <= 'Z';character++)
        {
            System.out.printf("Character %c has ascil value %d\n", character, ((int) character));
        }
            //Or, typecasting:
            System.out.println("InPrinting ASCII values 48-122 as characters:");

            for (num = 48; num <= 122; num++)
            {
                System.out.printf("ASCII value %d has character value %c\n", num, ((char) num));
            }

            //allow user input
            System.out.println("\nAllowing user ASCII value input: ");

                while (IsValidNum == false)
                    {
                        //check num double
                        System.out.print("Please enter ASCII value (32 - 127): "); 
                        if(sc.hasNextInt())
                        {
                            num = sc.nextInt(); 
                            IsValidNum = true;
                        }
                        else
                        {
                            System.out.println("Invalid integer--ASCII value must be a number. \n");
                        }
                        sc.nextLine(); // discard any other data entered on line
                        
                        // num data true, check num range
                        if (IsValidNum == true && num < 32 || num > 127)
                        {
                        System.out.println("ASCII value must be >= 32 and <= 127.\n");
                        IsValidNum = false; 
                        }
                        if(IsValidNum == true)
                        {
                        System.out.println();
                        System.out.printf("ASCII value %d has character value %c\n", num, ((char) num)); 
                        }
                    }
                    sc.close(); //close scanner
                }
            }