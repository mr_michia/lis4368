class Car extends Vehicle
{
    //instance variables (no static keyword): each object has own set of instance variables
private float speed;
//Java doesn't support parameters with default values (like C#, PHP, and C++)
//default constructor
public Car()
{
super();

System.out.println("\ninside car default constructor.");
//super®): //will generate error!
speed = 100;
}

//parameterized constructor
public Car(String m, String d, int y, float s)
{
super(m, d, y);
System.out.println( "\nInside car constructor with parameters.");
speed = s;
}

//getter/setter methods (accessor/mutator methods)
public double getSpeed()
{
return speed;
}

public void setSpeed (float s)
{
speed = s;
}
//subclass can override (replace) inherited method--that is, subclass's version of method
//overridden method must have *same* signature (i.e., same name, parameter list, and
public void print()
{

super.print();
System.out.println(", Speed: " + speed);
}
}