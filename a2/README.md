> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Adam Michia

### Assignment #2 Requirements:

*Sub-Heading:*

1. Install MySQL using HoneyBrew
2. Create Database Servlet
3. Update a2/index.jsp


#### README.md file should include the following items:

1. Links to:
   
    -[http://localhost:9999/hello](http://localhost:9999/hello "http://localhost:9999/hello")

    -[http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html "http://localhost:9999/hello/HelloHome.html")

    -[http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello "http://localhost:9999/hello/sayhello")

    -[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "http://localhost:9999/hello/querybook.html")


2. Screenshots of
    - querybook.html
    - query results
    - a2/index.jsp

#### Assignment Screenshots:

*Screenshot of querybook.html running http://localhost:9999/hello/querybook.html

![querybook.html Screenshot](img/database_connectivity1.png)



*Screenshot of querybook.html query results*:

![querybook.html query results Screenshot](img/database_connectivity2.png)


*Screenshot of a2/index.jsp*:

![a2/index.jsp Screenshot](img/a2index.png)



#### Skillset Screenshots:

*Screenshot of pset1.png running:*

![pset1.png Screenshot](img/pset1.png)

*Screenshot of pset2.png running:* 

![pset2.png Screenshot](img/pset2.png)

*Screenshot of pset3.png running:* 

![pset3.png Screenshot](img/pset3.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mr_michia/lis4368/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A2 My Team Quotes Tutorial Link](https://bitbucket.org/mr_michia/lis4368/src/master/README.md/ "My Team Quotes Tutorial")