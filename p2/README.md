> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Adam Michia

### Project #2 Requirements:

*Sub-Heading:*

1. Use JSP/Servlet web application to implement CRUD functionality.
2. Introduction of SCRUD option to search data.
3. Add associated screenshots of the web application.


#### README.md file should include the following items:

* Displays screenshots of pre-, post-valid user form entry, as well as MySQL customer table 
entry (see screenshots below).

#### Assignment Screenshots:

*Valid User Form Entry (customerform.jsp)*:

![customerform.jsp results Screenshot](img/1.png)


*Passed Validation (thanks.jsp) *:

![thanks.jsp results Screenshot](img/2.png)


*Display Data (customers.jsp) *:

![customers.jsp results Screenshot](img/3.png)


*Modify Form (modify.jsp) *:

![modify.jsp results Screenshot](img/4.png)


*Modified Data (customers.jsp) *:

![customers.jsp results Screenshot](img/5.png)


*Delete Warning (customers.jsp) *:

![customers.jsp results Screenshot](img/6.png)


*Associated Database Changes (Select, Insert, Update, Delete) *:

![Associated Database Changes results Screenshot](img/7.png)