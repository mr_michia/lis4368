> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Adam Michia

### Assignment #3 Requirements:

*Sub-Heading:*

1. Create a populated mySQL ERD
2. Includ 10 unique data records for each table
3. Update a3/index.jsp



#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1; 
2. Screenshot of ERD; 
3. Screenshot of a3/index.jsp 
4. Links to the following files: 
    - a3.mwb
    - a3.sql

#### Assignment Screenshots:

*Screenshot A3 ERD*: 
 
![A3 ERD](img/a3.png "ERD based upon A3 Requirements") 

*Screenshot A3 index.jsp*: 
 
![A3 index.png](img/index.png "index.jsp based upon A3 Requirements")
 
*A3 docs: a3.mwb and a3.sql*: 
 
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format") 
 
[A3 SQL File](docs/a3.sql "A3 SQL Script") 



#### Skillset Screenshots:

*Screenshot of pset4.png running:*

![pset4.png Screenshot](img/pset4.png)

*Screenshot of pset5.png running:* 

![pset5.png Screenshot](img/pset5.png)

*Screenshot of pset6.png running:* 

![pset6.png Screenshot](img/pset6.png)