> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web App Development

## Adam Michia

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
      (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL using HoneyBrew
    - Create Database Servlet
    - Update a2/index.jsp

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a populated mySQL ERD
    - Includ 10 unique data records for each table
    - Update a3/index.jsp

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Add the below server-side validation (see screenshots below) 
    - Compile the following class and servlet files: 
    - Use git to push *all* lis4368 files and changes to your remote course Bitbucket repo

1. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create ConnectionPool.java, CustomerDB.java, and DBUtil.java.
    - Compile all servlets. 
    - Provide screenshots of custoemrform.jsp, thanks.jsp, and database entry.
    - Provide skillsets 13-15.
  
2. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a webapp that mimics a virtual form
    - Implemented paramaters for each intake field of the webapp
    - Provide Failed/Passed Validation Screenshots

3. [P2 README.md](p2/README.md "My P2 README.md file")
    - Use JSP/Servlet web application to implement CRUD functionality.
    - Introduction of SCRUD option to search data.
    - Add associated screenshots of the web application.