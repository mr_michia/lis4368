> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web App Development

## Adam Michia

### Assignment #4 Requirements:

*Sub-Heading:*

1. Add the below server-side validation (see screenshots below) 
Important! (BOTH PC and Mac users): below, use *your* servlet-api.jar path accordingly. 
Compiling servlet files: 
Important! These are ONLY Examples: (*your* paths most likely will differ): 

2. Compile the following class and servlet files: 

    Windows: cd to C:\tomcat\webapps\lis4368\WEB-INF\classes 
    javac -cp . crud/business/Customer.java 
    javac -cp .;c:\tomcat\lib\servlet-api.jar crud/admin/CustomerServlet.java 
 
    Mac: cd to /Applications/tomcat/webapps/lis4368/WEB-INF/classes 
    javac -cp . crud/business/Customer.java 
    javac -cp .:/Applications/tomcat/lib/servlet-api.jar crud/admin/CustomerServlet.java 
 
1. Use git to push *all* lis4368 files and changes to your remote course Bitbucket repo



#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failed.png)


*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)






#### Skillset Screenshots:

*Screenshot of pset10.png running:*

![pset10.png Screenshot](img/pset10.png)

*Screenshot of pset11.png running:* 

![pset1.png Screenshot](img/pset11.png)

*Screenshot of pset12.png running:* 

![pset12.png Screenshot](img/pset12.png)